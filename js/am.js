/*

  *	assetmanager.js
	Back-end functionality for the RAGE Component Manager.

  * Copyright 2016-2018 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
	
  * Used services:
  
	GET <server>/assets ........................................... get list all assets
	GET <server>/search?q=<search-string> ......................... get list of assets search results
	GET <server>/assets/<asset-id>/metadata ....................... get asset metadata
	GET <server>/assets/<asset-id> ................................ get (download) asset package
	DELETE <server>/assets/<asset-id> ............................. delete asset
	POST <server>/assets .......................................... upload asset package
	POST <server>/assets?source=<asset-uri> ....................... create a clone of an asset
	POST <server>/assets?new ...................................... create a new empty asset
	GET <server>/assets/<asset-id>/artafacts/<artefact-id>/metadata ...... get artefact metadata
	GET <server>/assets/<asset-id>/artafacts/<artefact-id> ............... get (download) artefact as file
	DELETE <server>/assets/<asset-id>/artafacts/<artefact-id> ............ delete artefact
	POST <server>/assets/<asset-id/artefacts?section=<section> ........... upload artefact file
			
*/



var ASSET_MANAGER_NAME = 'assetmanager.html';
var ASSET_MANAGER_URL = '../Asset Manager/'+ASSET_MANAGER_NAME;

var items = {};
var itemList;
var navBar;
var searchString;
var showLocked = true;

// Global options extracted from document's URL
options = {};
function retrieveOptions()
{
	var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);

	while (match = search.exec(query))
	   options[decode(match[1])] = decode(match[2]);
}
retrieveOptions();


if (!options.page) options.page=1;

var PAGE_LIMIT = 10;
var PAGE_UNLIMITED = 1000000;
//var paging = {count:0, page:parseInt(options.page), offset:(parseInt(options.page)-1)*PAGE_LIMIT, limit:PAGE_LIMIT}; // only for the list of assets
var paging = {count:0, page:1, offset:0, limit:PAGE_LIMIT}; // only for the list of assets



/*	========================================================
	getJSONValue(title,defaultValue)

	Function. Retrieves the value of a multlingual title.
	Picks the English title, if available. Otherwise picks
	any title. If no data are present, return the default
	value.

	Parameter:
		A string holding JSON-LD value of a title, e.g.
		'[{"@value":"v1","@language":"en"},{...},{...},...]'
		
	Returns the value as string.
	========================================================
*/
function getJSONValue(title,defaultValue)
{
	// empty title
	if (!title || title.length==0)
		return defaultValue;
		
	var name = title[0]['@value'];
	
	// search for name in English
	for (var t in title)
		if (title[t]['@language']=='en')
		{
			name = title[t]['@value'];
			break;
		}
	return name;
}



/*	========================================================
	getJSONURI(value)

	Function. Retrieves the URI value of asset logo.

	Parameter:
		A string holding JSON-LD valuelike
		'[{"@uri":"..."}]'
		
	Returns the URI as string.
	========================================================
*/
function getJSONURI(value)
{
	// empty title
	if (!value || value.length==0)
		return '';
		
	var uri = value[0]['@uri'];
	return uri;
}



/*	========================================================
	initAssetManager()

	Command. Initializes the Asset Manager by creating the
	asset button section.

	Parameter:
		A string holding JSON-LD value of a title, e.g.
		'[{"@value":"v1","@language":"en"},{...},{...},...]'
		
	Returns the value as string.
	========================================================
*/
function initAssetManager()
{
	itemList = document.getElementById('itemList');
	navBar = document.getElementById('itemPages');
}



/*	========================================================
	getMetadataValue(dom, namespace, tag)

	Function. Returns the value of DOM element pointed by
	namespace and tag. 

	Parameter:
		dom			A DOM structure (DOM-parsed XML metadata)
		namespace	Namespace of the element
		tag			Tag of the element
		
	Returns the text as string. If the tag is missing,
	returns an empty string;
	========================================================
*/
function getMetadataValue(dom, namespace, tag)
{
	var elem = dom.getElementsByTagNameNS(namespace,tag);

	if (!elem) return '';
	if (!elem.length) return '';
	
	return elem[0].innerHTML; 
}



/*	========================================================
	getMetadataValueEn(dom, namespace, tag, parent)

	Function. Returns one of the translations in a multilingual
	DOM element pointed by namespace and tag, which is a child
	of a given parent element. Prefers translation in English.
	If not available, picks any other translation.

	Parameter:
		dom			A DOM structure (DOM-parsed XML metadata)
		namespace	Namespace of the element
		tag			Tag of the element
		parent		Tag of the element's parent 
		
	Returns the text as string or empty string if nothing
	is found.
	========================================================
*/
function getMetadataValueEn(dom, namespace, tag, parent)
{
	// get all tags
	var elems = dom.getElementsByTagNameNS(namespace,tag);
	if (!elems) return '';
	
	// filter only those in the given parent
	var elem = {};
	for (var i=0; i<elems.length; i++)
	{
		var p = elems[i].parentElement || elems[i].parentNode;
		if (p && p.localName==parent)
		{
			var lang = elems[i].getAttribute('xml:lang');
			elem[lang] = elems[i].innerHTML || elems[i].textContent;
		}
	}

	if (!elem) return '';
		
	// pick English translation (if not available, pick any
	// other translation
	return elem['en'] || elem[Object.keys(elem)[0]];
}



/*	========================================================
	firstSentence(text)
	
	Function. Attempts to retrieve only the first sentences
	of the text, with length.
	========================================================
*/
function firstSentence(text)
{
	var MAX_LENGTH = 300;

	// get the first sentence
	text = text.split('. ')[0];
	
	// it is short enought, we are happy
	var len = text.length; 
	if (len<MAX_LENGTH)
		return text+'.';
		
	// try to shorten word by word
	var words = text.split(' ');
	text = '';
	for (var i=0; i<words.length; i++)
	{
		if ((text.length+words[i].length)>MAX_LENGTH)
			break;
		text += words[i]+' ';
	}
	return text+'&hellip;';
}



/*	========================================================
	createAssetRowDOM(no,assetId,title,description,permissions,logo)

	Command. Creates a DOM row with asset title and
	description. Appends the row to the itemList element.
	========================================================
*/
function createAssetRowDOM(no,assetId,title,description,permissions,logo)
{
	// create asset row and insert it in the DOM
	var row = document.createElement('div');
	row.className = 'row';
	row.setAttribute('selected','false');
	row.addEventListener('click',selectAsset,false);
	row.assetId = assetId;
	itemList.appendChild(row);

	// asset logo (if any)
	if (logo)
	{
		// relative URI?
		if (logo.indexOf('http://')<0 && logo.indexOf('https://')<0)
			logo = RAGE_SERVER+logo;
			
		var elem = document.createElement('img');
		elem.className = 'asset-logo';
		elem.src = logo
		row.appendChild(elem);
		row.style.minHeight = '4.5em';
	}
	
	// asset title
	var elem = document.createElement('div');
	elem.className = 'name';
	elem.innerHTML = no+'. '+(permissions!='rw'?'<span class="locked">&#128274;</span> ':'')+title;
	row.appendChild(elem);
	
	// asset description
	var elem = document.createElement('div');
	elem.className = 'description';
	elem.innerHTML = firstSentence(description);
	row.appendChild(elem);
	
	// store asset information in a global array
	items[assetId] = {title:title, description:description, row:row, permissions:permissions, logo:logo};

	
	var bar = document.createElement('hr');
	bar.className = 'rowbar';
	itemList.appendChild(bar);

}



/*	========================================================
	getAssetsList()

	Command. Retrieves a list of all assets and creates the
	UI representation of these assets.
	========================================================
*/
function getAssetList()
{
	
	var listener = arguments.callee.toString().match(/function ([^\(]+)/)[1];

	function error(msg)
	{
		showMessage('Cannot retrieve the list of components.');
	}
	
	function timeout()
	{
		showMessage('Timeout - the server is slow or not responding.');
	}
	
	function success(data)
	{
//console.log('--------');
//console.log(data);	
		clearItems();
		
		var json = JSON.parse(data);
		if (json.length==0)
		{
			showMessage('No components are defined yet.');
			return;
		}
		
		paging.count = parseInt(getJSONValue(json[0]['http://rageproject.eu/2015/asset/count'],0))||0;
		paging.offset = parseInt(getJSONValue(json[0]['http://rageproject.eu/2015/asset/offset'],0))||0;
		paging.limit = parseInt(getJSONValue(json[0]['http://rageproject.eu/2015/asset/limit'],PAGE_LIMIT))||PAGE_LIMIT;
		createNavigationBar(listener);
		
		queryInfo = json.shift();

		var no = paging.offset+1;

		// walk through each asset in the asset list
		for (var i in json)
		{
			createAssetRowDOM(
				no++,
				json[i]['@id'],
				getJSONValue(json[i]['http://purl.org/dc/terms/title'],'No title'),
				getJSONValue(json[i]['http://purl.org/dc/terms/description'],'No description'),
				getJSONValue(json[i]['http://rageproject.eu/2015/asset/permissions'],'rw'),
				getJSONURI(json[i]['http://rageproject.eu/2015/asset/logo'])
			);
		}
	}
	
	document.getElementById('caption').innerHTML = 'RAGE Components '+lockButton();
	rage.getAssetsList({success: success, error: error, timeout: timeout, limit: paging.limit, offset: paging.offset});
}


function lockButton()
{
	return '';
	var s = '<span id="lock-icon" onclick="toggleLock()">'+(showLocked?'<img src="images/all.png" style="float:right">':'<img src="images/my.png" style="float:right">')+'</span>';
	return s;
}

function toggleLock()
{
	showLocked = !showLocked;
	if (showLocked)
		listAllAssets();
	else
		showMyAssets();
}

/*	========================================================
	selectAsset(event)

	Command. Selects or deselects an asset/artefact row.
	
	Parameters:
		event	DOM asset/artefact row
	========================================================
*/
function selectAsset(event)
{
	event.stopPropagation();

	// find the row
	var row = event.target;
	while (row && row.className!='row')
		row = row.parentElement;
	if (!row) return;

	var url = '../mew/vew.html?resource='+row.assetId+'&permissions='+items[row.assetId].permissions;
	window.location.href = (url);
}



/*	========================================================
	closeAllDialogs()

	Command. Hides all dialog windows.
	========================================================
*/
function closeAllDialogs()
{
	closeMessage();
	closeDeleteDialog();
	deselectRow();
}



/*	========================================================
	closeMessage()

	Command. Hides the message window.
	========================================================
*/
function closeMessage()
{
	document.getElementById('whitening').style.display = 'none';
	document.getElementById('message').style.display = 'none';
}



/*	========================================================
	showMessage(message)

	Command. Shows a given message in the message window.
	========================================================
*/
function showMessage(message)
{
	console.log('Message -',message);
	document.getElementById('messageButton').onclick = closeMessage;
	document.getElementById('messageText').innerHTML = message;
	document.getElementById('whitening').style.display = 'block';
	document.getElementById('message').style.display = 'block';
}


/*	========================================================
	searchAssetList()

	Command. Retrieves a list of all assets related to given
	search condition and creates the UI representation of
	these assets.
	
	The search string is in DOM element assetSearchString.
	If it is empty, show a list of all assets.
	========================================================
*/
function searchAssetList()
{
	// clear global array and DOM rows of assets
	showLocked = true;
	assets = [];
	itemList.innerHTML = '';
	items = {};
	//while (itemList.firstChild)
	//	itemList.removeChild(itemList.firstChild);
	
	// change title
	var searchString = document.getElementById('searchString').value.trim();

	var listener = arguments.callee.toString().match(/function ([^\(]+)/)[1];

	// no search string - then just show all assets
	if (!searchString)
	{
		getAssetList();
		return;
	}

	document.getElementById('caption').innerHTML = '<i>"'+searchString+'"</i> Components '+lockButton();
	document.getElementById('itemPages').innerHTML = '';
	
	function error(msg)
	{
		showMessage('Cannot retrieve the list of components.');
	}
	
	function timeout()
	{
		showMessage('Timeout - the server is slow or not responding.');
	}
	
	function success(data)
	{
		clearItems();
		
		var json = JSON.parse(data);
		if (json.length==0)
		{
			showMessage('No components are found matching "'+searchString+'".');
			return;
		}

		paging.count = parseInt(getJSONValue(json[0]['http://rageproject.eu/2015/asset/count'],0))||0;
		paging.offset = parseInt(getJSONValue(json[0]['http://rageproject.eu/2015/asset/offset'],0))||0;
		paging.limit = parseInt(getJSONValue(json[0]['http://rageproject.eu/2015/asset/limit'],PAGE_LIMIT))||PAGE_LIMIT;
		createNavigationBar(listener);

		queryInfo = json.shift();
		
		var no = paging.offset+1;
		
		// walk through each asset in the asset list
		for (var i in json)
		{
			createAssetRowDOM(
				no++,
				json[i]['@id'],
				getJSONValue(json[i]['http://purl.org/dc/terms/title'],'No title'),
				getJSONValue(json[i]['http://purl.org/dc/terms/description'],'No description'),
				getJSONValue(json[i]['http://rageproject.eu/2015/asset/permissions'],'rw'),
				getJSONURI(json[i]['http://rageproject.eu/2015/asset/logo'])
			);
		}
	}
	
	rage.searchAssetsList({success: success, error: error, timeout: timeout, search: searchString, limit: paging.limit, offset: paging.offset});
}



/*	========================================================
	showMyAssets()

	Command. Retrieves a list of all assets and shows only
	the one with RW permissions.
	========================================================
*/
function showMyAssets()
{
	// clear global array and DOM rows of assets
	assets = [];
	itemList.innerHTML = '';
	items = {};
	
	document.getElementById('caption').innerHTML = 'My Components '+lockButton();
	
	function error(msg)
	{
		showMessage('Cannot retrieve the list of components.');
	}
	
	function timeout()
	{
		showMessage('Timeout - the server is slow or not responding.');
	}
	
	function success(data)
	{
		clearItems();
		
		var json = JSON.parse(data);
		if (json.length==0)
		{
			showMessage('No components are found matching "'+searchString+'".');
			return;
		}

		queryInfo = json.shift();
		
		var no = 1;
		
		// walk through each asset in the asset list
		for (var i in json)
		{
			var permissions = getJSONValue(json[i]['http://rageproject.eu/2015/asset/permissions'],'rw');
			if (permissions == 'rw')
			{
				createAssetRowDOM(
					no++,
					json[i]['@id'],
					getJSONValue(json[i]['http://purl.org/dc/terms/title'],'No title'),
					getJSONValue(json[i]['http://purl.org/dc/terms/description'],'No description'),
					permissions,
					getJSONURI(json[i]['http://rageproject.eu/2015/asset/logo'])
				);
			}
		}
	}
	
	rage.getAssetsList({success: success, error: error, timeout: timeout, limit: PAGE_UNLIMITED, offset: 0});
}



/*	========================================================
	fileSize(size)

	Function. Converts a files size in a shorter format:
	xxxxb, xxx.xkB, xxx.xMB or xxxMB. 
	
	Parameter:
		size	file size in bytes
		
	Outputs the formatted files size as a string
	========================================================
*/
function fileSize(size)
{
	if (size<1024)
		return size+'b';
		
	if (size<10*1024)
		return (size/1024).toFixed(1)+'kB';
		
	if (size<10*1024*1024)
		return (size/1024/1024).toFixed(1)+'MB';

	return (size/1024/1024).toFixed(0)+'MB';
}



/*	========================================================
	gotoViewAssets()

	Command. Navigates to the main Asset Manager page.
	========================================================
*/
function gotoViewAssets()
{
	document.location.href='am.html';
}



/*	========================================================
	newAsset()

	Command. Creates a new asset.
	========================================================
*/
function newAsset()
{
	function error()
	{
		showMessage('Could not create a new component.');
		rage.hideLoader();
	}
	
	function success(data)
	{
		console.log('new component',data);
		rage.hideLoader();

		// try to convert data from:
		//		https://rage-repo.it.fmi.uni-sofia.bg:8443/fcrepo/rest/assets/b7f450a1-82b5-4b3b-bf67-73e53670dd46
		// into:
		//		b7f450a1-82b5-4b3b-bf67-73e53670dd46
		data = data.split('/');
		data = data[data.length-1];
		
		window.location.href = '../mew/vew.html?resource='+data+'&assetId='+data+'&permissions=rw&back='+encodeURIComponent('../am/am.html');
	}
	
	rage.showLoader('Creating component');
	rage.newAsset({success: success, error: error});
}



/*	========================================================
	uploadAsset(file)

	Command. Selects and uploads an asset package.
	========================================================
*/
function confirmUploadAsset()
{
	var inst = $('[data-remodal-id=upload]').remodal();
	inst.open();
	
	document.getElementById('upload-ok').onclick = function ()
	{
		var button = document.getElementById('file-select')
		button.onchange = function (event)
		{
			var files = event.target.files;
			if (files.length<1) return;
			uploadAsset(files[0]);
		}
		
		button.click();
	};
}

function uploadAsset(file)
{
	//var html = '<p>Uploading: <b>'+file.name+'</b>';
	//html += '<br><small>(<span id="uploadProgress"></span>';
	//html += fileSize(file.size)+(file.type?', '+file.type:'')+')</small></p>';
	//document.getElementById('fileMessage').innerHTML = html;
	//document.getElementById('fileMessage').style.display = 'block';
	console.log('uploading',file);
	
	function error(message)
	{
		rage.hideLoader();
		//dropZone.busy = false;
		//document.getElementById('fileMessage').innerHTML += '<p>Could not upload the component package.</p>';
		console.log('Could not upload the component package. ['+message+']');
	}
	
	//function progress(event)
	//{
	//	if (event.lengthComputable)
	//		html = fileSize(event.loaded)+' of ';
	//	else
	//		html = 'unknown size';
	//	document.getElementById('uploadProgress').innerHTML = html;
	//}
	
	function success(data)
	{
		rage.hideLoader();
		
		console.log('uploaded component',data);

		if (data)
		{
			// try to convert data from:
			//		https://rage-repo.it.fmi.uni-sofia.bg:8443/fcrepo/rest/assets/b7f450a1-82b5-4b3b-bf67-73e53670dd46
			// into:
			//		b7f450a1-82b5-4b3b-bf67-73e53670dd46
			data = data.split('/');
			data = data[data.length-1];

			var url = '../mew/vew.html?resource='+data+'&permissions=rw';
			window.location.href = (url);
		}
		
		console.log('uploading done');
	}
	
	rage.showLoader('Uploading package');
	rage.uploadAsset({success: success, error: error, /*progress: progress,*/ size:file.size, file:file});
}


/*	========================================================
	clearItems()

	Command. Clears the list of shown items.
	========================================================
*/
function clearItems()
{
	items = {};
	itemList.innerHTML = '';
	navBar.innerHTML = '';
}


/*	========================================================
	createNavigationBar()

	Command. Generates the navigation bar for paginagion.
	Uses paging stucture.
	========================================================
*/
function createNavigationBar(listener)
{
	// no need for navigation
	if (paging.offset==0 && paging.count<=paging.limit)
	{
		navBar.innerHTML = '';
		return;
	}
	
	navBar.innerHTML = 'Page: ';
	if (paging.limit < 0) 
	{
		paging.limit = PAGE_LIMIT;
	}
	page=1;
	var that = this;
	for (var ofs=0; ofs<paging.count; ofs+=paging.limit, this.page++)
	{
		var button = document.createElement('a');
		if (ofs==paging.offset) button.setAttribute('current',0);
		button.className = "navButton";
		button.innerHTML = page;
		button.listener = listener;
		button.page = page;
		button.setAttribute('href','#');
		button.addEventListener('click',showPage);
		navBar.appendChild(button);
		
	}
}




/*	========================================================
	showPage(event)

	Command. Requests items for a given page.
	========================================================
*/
function showPage(event)
{
	var page = event.target.page||0;
	if (!page) return;
	
	paging.page = page;
	paging.offset = (page-1)*PAGE_LIMIT;
	var fn = window[event.target.listener];
	if(typeof fn === 'function') {
    	fn();
	}
}

function resetPaging()
{
	paging.count = 0;
	paging.offset = 0;
	paging.limit = PAGE_LIMIT;
}